﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SDKDEMOConsola
{
    class Program
    {
        static void Main(string[] args)
        {
            bool empresaAbierta = false;
            int result = 0;
            string szRegKeySistema = @"SOFTWARE\\Computación en Acción, SA CV\\CONTPAQ I COMERCIAL";            
            RegistryKey keySistema = Registry.LocalMachine.OpenSubKey(szRegKeySistema);
            Libreria.SetCurrentDirectory(keySistema.GetValue("DirectorioBase").ToString());
            result = Libreria.FSetNombrePAQ("CONTPAQ I COMERCIAL");

            if (result != 0)
            {
                Console.WriteLine(Libreria.RError(result));
            }
            else
            {
                Console.WriteLine("Inicio de sesión éxitoso");
            }

            ///Se obtienen las empresas
            List<Empresa> empresas = new List<Empresa>();
            var id = 0;
            var nombre = new StringBuilder(Libreria.kLongNombre);
            var ruta = new StringBuilder(Libreria.kLongRuta);
            result = Libreria.FPosPrimerEmpresa(ref id, nombre, ruta);
            if (result == 0)
            {
                Empresa empresa = new Empresa()
                {
                    Id = id,
                    Nombre = nombre.ToString(),
                    Ruta = ruta.ToString()
                };
                empresas.Add(empresa);
                while (Libreria.FPosSiguienteEmpresa(ref id, nombre, ruta) == 0)
                {
                    empresa = new Empresa()
                    {
                        Id = id,
                        Nombre = nombre.ToString(),
                        Ruta = ruta.ToString()
                    };

                    empresas.Add(empresa);
                }
            }
            else
            {
                Console.WriteLine("No se encontraron empresas");                
            }

            if (empresas.Count > 0)
            {
                foreach (var empresa in empresas)
                {
                    Console.WriteLine("Identificador: {0} || Nombre: {1}", empresa.Id ,empresa.Nombre);
                }

                Console.WriteLine("Ingrese el identificador de la empresa que desee abrir");
                bool esNumerico = false;
                int identificador = 0;
                while(esNumerico == false)
                {
                    string identificadorSeleccinado = Console.ReadLine();
                    esNumerico = int.TryParse(identificadorSeleccinado, out identificador);
                    if (!esNumerico)
                    {
                        Console.WriteLine("Ingrese un valor númerico");
                    }                   
                }                

                var empresaSeleccionada = empresas.FirstOrDefault(emp => emp.Id == identificador);
                result = Libreria.FAbreEmpresa(empresaSeleccionada.Ruta);

                if (result != 0)
                {
                    Console.WriteLine(Libreria.RError(result));
                }
                else
                {
                    Console.WriteLine("Empresa abierta con éxito");
                    empresaAbierta = true;
                    int option = 0;
                    while (option != 10)
                    {                        
                        Console.WriteLine("====================================================================");
                        Console.WriteLine("¿Qué es lo que desea hacer?");
                        Console.WriteLine("-1;Ver lista de productos");
                        Console.WriteLine("-2;Buscar producto por código");
                        Console.WriteLine("-3;Ingresar nuevo producto");
                        Console.WriteLine("-4;Editar producto");
                        Console.WriteLine("-5;Eliminar producto");
                        Console.WriteLine("-10;Salir");
                        Console.WriteLine("====================================================================");
                        string opcionSeleccionada = Console.ReadLine();
                        esNumerico = int.TryParse(opcionSeleccionada, out option);
                        if (!esNumerico)
                        {
                            Console.WriteLine("Ingrese un valor númerico");
                        }
                        else
                        {
                            switch (option)
                            {
                                case 1:
                                    List<Producto> productos = new List<Producto>();
                                    Libreria.FPosPrimerProducto();
                                    var idProducto = new StringBuilder(12);
                                    var codigoProducto = new StringBuilder(Libreria.kLongCodigo);
                                    var nombreProducto = new StringBuilder(Libreria.kLongNombre);
                                    Libreria.FLeeDatoProducto("CIDPRODUCTO", idProducto, 12);
                                    Libreria.FLeeDatoProducto("CCODIGOPRODUCTO", codigoProducto, Libreria.kLongCodigo);
                                    Libreria.FLeeDatoProducto("CNOMBREPRODUCTO", nombreProducto, Libreria.kLongNombre);
                                    productos.Add(new Producto()
                                    {
                                        Id = int.Parse(idProducto.ToString()),
                                        Codigo = codigoProducto.ToString(),
                                        Nombre = nombreProducto.ToString()
                                    });
                                    while (Libreria.FPosSiguienteProducto() == 0)
                                    {
                                        idProducto = new StringBuilder(12);
                                        codigoProducto = new StringBuilder(Libreria.kLongCodigo);
                                        nombreProducto = new StringBuilder(Libreria.kLongNombre);
                                        Libreria.FLeeDatoProducto("CIDPRODUCTO", idProducto, 12);
                                        Libreria.FLeeDatoProducto("CCODIGOPRODUCTO", codigoProducto, Libreria.kLongCodigo);
                                        Libreria.FLeeDatoProducto("CNOMBREPRODUCTO", nombreProducto, Libreria.kLongNombre);
                                        productos.Add(new Producto()
                                        {
                                            Id = int.Parse(idProducto.ToString()),
                                            Codigo = codigoProducto.ToString(),
                                            Nombre = nombreProducto.ToString()
                                        });                                        
                                    }
                                    foreach (var producto in productos)
                                    {
                                        Console.WriteLine("Código: {0} || Nombre: {1}", producto.Codigo, producto.Nombre);
                                    }
                                    break;
                                case 2:
                                    Console.WriteLine("Ingrese código de producto");                                    
                                    string codigo = Console.ReadLine();
                                    result = Libreria.FBuscaProducto(codigo);
                                    if (result != 0)
                                    {
                                        Console.WriteLine(Libreria.RError(result));
                                    }
                                    else
                                    {
                                        idProducto = new StringBuilder(12);
                                        codigoProducto = new StringBuilder(Libreria.kLongCodigo);
                                        nombreProducto = new StringBuilder(Libreria.kLongNombre);
                                        Libreria.FLeeDatoProducto("CIDPRODUCTO", idProducto, 12);
                                        Libreria.FLeeDatoProducto("CCODIGOPRODUCTO", codigoProducto, Libreria.kLongCodigo);
                                        Libreria.FLeeDatoProducto("CNOMBREPRODUCTO", nombreProducto, Libreria.kLongNombre);
                                        var product = new Producto()
                                        {
                                            Id = int.Parse(idProducto.ToString()),
                                            Codigo = codigoProducto.ToString(),
                                            Nombre = nombreProducto.ToString()
                                        };
                                        Console.WriteLine("Código: {0} || Producto: {1}", product.Codigo, product.Nombre);
                                    }
                                    break;
                                case 3:                                    
                                    Libreria.tProducto tProducto = new Libreria.tProducto();
                                    id = 0;
                                    Console.WriteLine("Ingrese el código de producto");
                                    tProducto.cCodigoProducto = Console.ReadLine();
                                    Console.WriteLine("Ingrese nombre del producto");
                                    tProducto.cNombreProducto = Console.ReadLine();                                    
                                    result = Libreria.FAltaProducto(ref id, ref tProducto);
                                    if (result != 0)
                                    {
                                        Console.WriteLine(Libreria.RError(result));
                                    }
                                    else
                                    {
                                        Console.WriteLine("Identificador producto ingresado: {0}", id);
                                    }
                                    break;
                                case 4:
                                    Console.WriteLine("Ingrese el código del producto que desee modificar");
                                    result = Libreria.FBuscaProducto(Console.ReadLine());
                                    if (result != 0)
                                    {
                                        Console.WriteLine(Libreria.RError(result));
                                    }
                                    else
                                    {
                                        Console.WriteLine("Ingrese nombre de producto");
                                        Libreria.FEditaProducto();
                                        result = Libreria.FSetDatoProducto("CNOMBREPRODUCTO", Console.ReadLine());
                                        if (result != 0)
                                        {
                                            Console.WriteLine(Libreria.RError(result));                                            
                                        }
                                        else
                                        {
                                            result = Libreria.FGuardaProducto();
                                            if (result != 0)
                                            {
                                                Console.WriteLine(Libreria.RError(result));
                                            }
                                            else
                                            {
                                                Console.WriteLine("Producto modificado");
                                            }
                                        }
                                    }
                                    break;
                                case 5:
                                    Console.WriteLine("Ingrese el código del producto que desee eliminar");
                                    result = Libreria.FEliminarProducto(Console.ReadLine());
                                    if (result != 0)
                                    {
                                        Console.WriteLine(Libreria.RError(result));                                       
                                    }
                                    else
                                    {
                                        Console.WriteLine("Producto eliminado con éxito");
                                    }
                                    break;
                                default:
                                    break;
                            }
                        }
                    }            
                }
            }



            Console.ReadLine();
            if (empresaAbierta)
            {
                Libreria.FCierraEmpresa();
            }
            Libreria.FTerminaSDK();
        }
    }
}
