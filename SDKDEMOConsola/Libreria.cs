﻿using System.Runtime.InteropServices;
using System.Text;

namespace SDKDEMOConsola
{
    public static class Libreria
    {
        #region CONEXION
        /// <summary>
        /// Se conecta con el SDK de Contpaqi Comercial
        /// </summary>
        /// <param name="aNombrePAQ"></param>
        /// <returns>Resultado de la operación en código</returns>
        [DllImport("MGWSERVICIOS.DLL", EntryPoint = "fSetNombrePAQ")]
        public static extern int FSetNombrePAQ(string aNombrePAQ);

        /// <summary>
        /// Termina la conexión con el SDK
        /// </summary>
        [DllImport("MGWSERVICIOS.DLL", EntryPoint = "fTerminaSDK")]
        public static extern void FTerminaSDK();

        /// <summary>
        /// Convierte el código del resultado de alguna
        /// operación y lo convierte en mensaje con
        /// ayuda de <see cref="RError(int)"/>
        /// </summary>
        /// <param name="NumeroError"></param>
        /// <param name="Mensaje"></param>
        /// <param name="Longitud"></param>
        [DllImport("MGWSERVICIOS.DLL", EntryPoint = "fError")]
        public static extern void FError(int NumeroError, StringBuilder Mensaje, int Longitud);

        /// <summary>
        /// Establece la dirección de donde se encuantra
        /// el SDK, generalmente donde Comercial esta instalado
        /// </summary>
        /// <param name="pPtrDirActual"></param>
        /// <returns></returns>
        [DllImport("KERNEL32")]
        public static extern int SetCurrentDirectory(string pPtrDirActual);

        /// <summary>
        /// Pasa el código del error y lo convierte
        /// en un mensaje entendible
        /// </summary>
        /// <param name="iError">El código del error</param>
        /// <returns>El error en mensaje</returns>
        public static string RError(int iError)
        {
            StringBuilder sMensaje = new StringBuilder(512);
            string msg = string.Empty;
            if (iError != 0)
            {
                FError(iError, sMensaje, 512);
                msg = "Error: " + sMensaje;
            }

            return msg;
        }
        #endregion

        #region EMPRESA
        /// <summary>
        /// Esta función abre la empresa que corresponde a la ruta especificada en el parámetro aDirectorioEmpresa.     
        /// </summary>
        /// <param name="Directorio">Ruta de la empresa Contpaqi Comercial.</param>
        /// <returns>Resultado de la operación en código</returns>
        [DllImport("MGWSERVICIOS.DLL", EntryPoint = "fAbreEmpresa")]
        public static extern int FAbreEmpresa(string Directorio);

        /// <summary>
        /// Cierra la conexión con la empresa activa en la aplicación que usa el SDK.
        /// </summary>
        [DllImport("MGWSERVICIOS.DLL", EntryPoint = "fCierraEmpresa")]
        public static extern void FCierraEmpresa();
        #endregion
        #region Navegación
        /// <summary>
        /// Esta función se posiciona en el primer registro de la base de datos de empresas de CONTPAQi® Comercial, 
        /// modifica los parámetros aNombreEmpresa y aDirectorioEmpresa, 
        /// en los cuales guarda el nombre de la primera empresa y su ruta, 
        /// correspondientemente.
        /// </summary>
        /// <param name="lIdEmpresa">Al finalizar la función este parámetro contiene el identificador de la primera empresa registrada en la Base de Datos.</param>
        /// <param name="lNombreEmpresa">Al finalizar la función este parámetro contiene el nombre de la primera empresa registrada en la Base de Datos.</param>
        /// <param name="lDirectorioEmpresa">Al finalizar la función este parámetro contiene el directorio de la primera empresa registrada en la base de datos.</param>
        /// <returns>Resultado en código</returns>
        [DllImport("MGWServicios.dll", EntryPoint = "fPosPrimerEmpresa")]
        public static extern int FPosPrimerEmpresa(ref int lIdEmpresa, StringBuilder lNombreEmpresa, StringBuilder lDirectorioEmpresa);

        /// <summary>
        /// Esta función avanza al siguiente registro en la tabla de Empresas de CONTPAQi® Comercial; 
        /// en caso de que no exista un siguiente registro, la función retorna un valor distinto de 0 (cero).
        /// </summary>
        /// <param name="aIdEmpresa">Al finalizar la función este parámetro contiene el identificador de la siguiente empresa registrada en la Base de Datos.</param>
        /// <param name="aNombreEmpresa">Al finalizar la función este parámetro contiene el nombre de la siguiente empresa registrada en la base de datos.</param>
        /// <param name="aDirectorioEmpresa">Al finalizar la función este parámetro contiene el directorio de la siguiente empresa registrada en la base de datos.</param>
        /// <returns>Resultado en código</returns>
        [DllImport("MGWServicios.dll", EntryPoint = "fPosSiguienteEmpresa")]
        public static extern int FPosSiguienteEmpresa(ref int aIdEmpresa, StringBuilder aNombreEmpresa, StringBuilder aDirectorioEmpresa);
        #endregion

        #region PRODUCTOS
        /// <summary>
        /// Esta función busca un producto por su código.
        /// </summary>
        /// <param name="aCodProducto">Código del producto.</param>
        /// <returns></returns>
        [DllImport("MGWSERVICIOS.DLL", EntryPoint = "fBuscaProducto")]
        public static extern int FBuscaProducto(string aCodProducto);

        /// <summary>
        /// Esta función se ubica en el primer registro de la tabla de Productos.
        /// </summary>
        /// <returns></returns>
        [DllImport("MGWSERVICIOS.DLL", EntryPoint = "fPosPrimerProducto")]
        public static extern int FPosPrimerProducto();

        /// <summary>
        /// Esta función se ubica en el siguiente registro de la posición actual de la tabla de Productos.
        /// </summary>
        /// <returns></returns>
        [DllImport("MGWSERVICIOS.DLL", EntryPoint = "fPosSiguienteProducto")]
        public static extern int FPosSiguienteProducto();

        /// <summary>
        /// Esta función lee el valor indicado del campo correspondiente en el registro activo de la tabla de productos.
        /// </summary>
        /// <returns></returns>
        [DllImport("MGWSERVICIOS.DLL", EntryPoint = "fLeeDatoProducto")]
        public static extern int FLeeDatoProducto(string aCampo, StringBuilder aValor, int aLen);

        [DllImport("MGWServicios.dll", EntryPoint = "fAltaProducto")]
        public static extern int FAltaProducto(ref int aIdProducto, ref tProducto astProducto);

        [DllImport("MGWServicios.dll", EntryPoint = "fEditaProducto")]
        public static extern int FEditaProducto();

        [DllImport("MGWServicios.dll", EntryPoint = "fSetDatoProducto")]
        public static extern int FSetDatoProducto(string aCampo, string aValor);

        [DllImport("MGWServicios.dll", EntryPoint = "fGuardaProducto")]
        public static extern int FGuardaProducto();

        [DllImport("MGWServicios.dll", EntryPoint = "fEliminarProducto")]
        public static extern int FEliminarProducto(string aCodigoProducto);
        #endregion

        #region CONSTANTES
        public const int kLogitudLugarExpedicion = 401;
        public const int kLongAbreviatura = 4;
        public const int kLongCadOrigComplSAT = 501;
        public const int kLongCodigo = 31;
        public const int kLongCodigoPostal = 7;
        public const int kLongCodValorClasif = 4;
        public const int kLongCuenta = 101;
        public const int kLongCURP = 21;
        public const int kLongDenComercial = 51;
        public const int kLongDesCorta = 21;
        public const int kLongDescripcion = 61;
        public const int kLongEmailWeb = 51;
        public const int kLongFecha = 24;
        public const int kLongFechaHora = 36;
        public const int kLongitudFolio = 17;
        public const int kLongitudMoneda = 61;
        public const int kLongitudMonto = 31;
        public const int kLongitudRegimen = 101;
        public const int kLongitudUUID = 37;
        public const int kLongMensaje = 3001;
        public const int kLongNombre = 61;
        public const int kLongNombreProducto = 256;
        public const int kLongNumeroExpandido = 31;
        public const int kLongNumeroExtInt = 7;
        public const int kLongReferencia = 21;
        public const int kLongRepLegal = 51;
        public const int kLongRFC = 21;
        public const int kLongRuta = 254;
        public const int kLongSelloCFDI = 176;
        public const int kLongSelloSat = 176;
        public const int kLongSerie = 12;
        public const int kLongTelefono = 16;
        public const int kLongTextoExtra = 51;
        public const int kLonSerieCertSAT = 21;
        #endregion

        #region ESTRUCTURAS
        /// <summary>
        /// Estructura producto/servicio
        /// </summary>
        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi, Pack = 4)]
        public struct tProducto
        {
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = kLongCodigo)]
            public string cCodigoProducto;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = kLongNombre)]
            public string cNombreProducto;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = kLongNombreProducto)]
            public string cDescripcionProducto;
            public int cTipoProducto;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = kLongFecha)]
            public string cFechaAltaProducto;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = kLongFecha)]
            public string cFechaBaja;
            public int cStatusProducto;
            public int cControlExistencia;
            public int cMetodoCosteo;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = kLongCodigo)]
            public string cCodigoUnidadBase;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = kLongCodigo)]
            public string cCodigoUnidadNoConvertible;
            public double cPrecio1;
            public double cPrecio2;
            public double cPrecio3;
            public double cPrecio4;
            public double cPrecio5;
            public double cPrecio6;
            public double cPrecio7;
            public double cPrecio8;
            public double cPrecio9;
            public double cPrecio10;
            public double cImpuesto1;
            public double cImpuesto2;
            public double cImpuesto3;
            public double cRetencion1;
            public double cRetencion2;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = kLongNombre)]
            public string cNombreCaracteristica1;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = kLongNombre)]
            public string cNombreCaracteristica2;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = kLongNombre)]
            public string cNombreCaracteristica3;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = kLongCodValorClasif)]
            public string cCodigoValorClasificacion1;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = kLongCodValorClasif)]
            public string cCodigoValorClasificacion2;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = kLongCodValorClasif)]
            public string cCodigoValorClasificacion3;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = kLongCodValorClasif)]
            public string cCodigoValorClasificacion4;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = kLongCodValorClasif)]
            public string cCodigoValorClasificacion5;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = kLongCodValorClasif)]
            public string cCodigoValorClasificacion6;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = kLongTextoExtra)]
            public string cTextoExtra1;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = kLongTextoExtra)]
            public string cTextoExtra2;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = kLongTextoExtra)]
            public string cTextoExtra3;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = kLongFecha)]
            public string cFechaExtra;
            public double cImporteExtra1;
            public double cImporteExtra2;
            public double cImporteExtra3;
            public double cImporteExtra4;
        }
        #endregion
    }
}
